---
title: "Cheatsheet"
author: Severin Kaderli
...

# Signals
## Entropy
$m$ = number of possible states  
$n$ = length of message  
$p_{x}$ = probability of occurence  
$I_{x}$ = self-information of symbol $x$  

$$
I_{x} = -log_{m}(p_{x})
$$

$I_{tot}$ = self-information of a message

$$
I_{t} = \sum_{k=1}^{n}I_{xk}
$$

$H$ = entropy  

$$
H = \frac{I_{tot}}{n}
$$

$H_{2}$ = entropy in bits

$$
H_{2} = H \cdot log_{2}(m)
$$

## Data Rate
$B$ = bandwidth  
$V$ = number of discrete levels  
$D_{max}$ = maximum data rate

$$
D_{max} = 2 \cdot B \cdot log_{2}(V)
$$

## Signal-to-Noise Ratio
$SNR$ = Signal-to-Noise Ratio

$$
SNR = 10\cdot log_{10}(\frac{S}{N})
$$

# Ethernet
## OSI Model
1. Physical
2. Data Link
3. Network
4. Transport
5. Session
6. Presentation
7. Application

## Hubs
Messages arriving on a port are regenerated and broadcast to every other port.
Hubs are working on Layer 1. All devices connected to a hub are in the same
collision domain.

## Switches
Switches store which hosts are connected on which port in a table. This allows a
colission free operation. They are working on layer 2. All devices inside a
switched network are inside the same broadcast domain. A bridge is a switch with
only two ports. A switch build its switching table using the mac addresses of
the incoming frames.

## Spanning Tree Protocol (SPT)
An algorithm to prevent loops on layer 2. Its goal is the deactivate redundant
paths.

## VLANs
Ethernet frames are tagged with a VLAN tag to mark the frame as belonging to a 
particular VLAN.

**Untagged:**  
* Can only be assigned one VLAN
* Inbound frames are tagged
* Outbound frames are untagged

**Tagged:**  
* Can be assigned multiple VLANs
* Doesn't remove tags

## Frames
![](./assets/frame.png)

# Addressing
## IP-Address
An IP address contains a network number and a host number.

## Subnetting
$n$ = $n$ bits for network  
$h$ = number of bits for hosts  

$$
h := 32 - n
$$

$H$ = number of adresses available for hosts

$$
H := 2^{h} - 2
$$

![](./assets/subnetting.png)

# Address Resolution Protocol (ARP)
Translates local protocol addresses to hardware addresses.

1. ARP-Request as Broadcast
2. Get answer directly
3. Save address in cache

![](./assets/arp.png)

OPER = 1 for request, 2 for response

# ICMP
This is the error reporting and diagnostic mechanism of the internet.

![](assets/icmp.png)